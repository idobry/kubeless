#!/bin/bash

docker build ~/Desktop/kubeless/main -t idobry/main:$1
docker push idobry/main:$1

kubectl apply -f deploy-all.yaml

#docker build ~/Desktop/kubeless/worker -t idobry/worker:$1
#docker push idobry/worker:$1
