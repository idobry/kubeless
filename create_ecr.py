import sys
import os
import yaml
import boto3
import docker
import base64

# python create_ecr.py /path/to/files <src_env> <region> <dst_env> <region>
# python create_ecr.py . lab00 us-east-1 lab01 eu-west-1

os.chdir(sys.argv[1])
src_env = sys.argv[2]
dst_env = sys.argv[4]
aws_dst = boto3.client('ecr', region_name=sys.argv[5])
aws_src = boto3.client('ecr', region_name=sys.argv[3])

docker_client = docker.from_env()

def login_to_docker_registry(aws_client):
    docker_client = docker.from_env()
    resp = docker_client.login(username='AWS',
                               email='none', password=get_token(aws_client), registry=get_registry(aws_client))
    if not resp:
        raise Exception("Not able to login")                         
    return docker_client

 
def get_registry(aws_client):
    registry_endpoint = aws_client.get_authorization_token()['authorizationData'][0]['proxyEndpoint']
    return registry_endpoint

 
def get_token(aws_client):
    token = base64.b64decode(aws_client.get_authorization_token()['authorizationData'][0]['authorizationToken']).split(':')[1]
    return token


if __name__ == "__main__":
    with open('values.yaml') as values_file, open(src_env + '-values.yaml') as version_file:
        try:
            #loaf yaml files
            values = yaml.load(values_file)
            src_values = yaml.load(version_file)
            #get dns names
            ecr_src_dns = aws_src.get_authorization_token()['authorizationData'][0]['proxyEndpoint'].split('//')[1]
            ecr_dst_dns = aws_dst.get_authorization_token()['authorizationData'][0]['proxyEndpoint'].split('//')[1]
            #login to docker registry
            docker_client_src = login_to_docker_registry(aws_src)
            docker_client_dst = login_to_docker_registry(aws_dst)

            for service in values['components']:                
                #build image name
                name = values['components'][service]['imageName']
                if 'detection-server-api' not in name:
                    version = src_values['components'][service]['imageTag']  
                    image_to_push = ecr_dst_dns + '/' + dst_env + '/' + name + ':' + version
                    image_to_pull = ecr_src_dns + '/' + src_env + '/' + name + ':' + version
                    #create repo for new image
                    aws_dst.create_repository(repositoryName=dst_env + '/' + name)
                    #pull old image from src repo
                    print "pulling : " + image_to_pull
                    pull_ans = docker_client_src.images.pull(image_to_pull)                
                    if not pull_ans:
                        raise Exception("Not able to pull image " + image_to_pull)
                    #retag old image
                    tag_ans = docker_client_src.api.tag(image_to_pull, image_to_push, version, force=True)
                    if not tag_ans:
                        raise Exception("Not able to retage image " + image_to_pull)
                    #push new image to new dst repo
                    push_ans = docker.from_env().images.push(image_to_push, version)
                    if not push_ans:
                        raise Exception("Not able to push image " + image_to_push)

                    print "successfully created and pushed " + image_to_push

        except yaml.YAMLError as exc:
            print(exc)
        except Exception as e:
            print('**ERROR**:' + str(e))
